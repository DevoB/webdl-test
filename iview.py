from common import grab_json, grab_xml, Node, download_hls, download_http
import requests_cache
import urllib.parse
import logging
import os
import json
import re

API_URL = "https://iview.abc.net.au/api"
AUTH_URL = "https://iview.abc.net.au/auth"

def format_episode_title(series, ep):
    if ep:
        return series + " " + ep
    else:
        return series

def descriptive_filename(series, title, urlpart):
    """Generates a more descriptive file name from the programme title"""
    # if title contains program, remove duplication
    title = title.replace(series + ' ', '')

    # for specials that title == program, just use program.ext
    if series == title:
        filename = series
    else:
        # If we can get a SxEy show descriptor, lets use it.
        # 1) Se(ries) X Ep(isope) Y Actual Title [iView common format]
        match1 = re.match(r"(Se\D*) (\d+) (Ep\D*) (\d+) (.*)", title)
        # 2) Se(ries) X Ep(isope) Y		[Some iView episodes]
        match2 = re.match(r"(Se\D*) (\d+) (Ep\D*) (\d+)", title)
		# 3) filename_X_Y_	[Seen at ABC iview]
        match3 = re.match(r".*?_(\d+)_(\d+)", urlpart)

        if match1:
            title = 'S{:0>2}E{:0>2} - {}'.format(match1.group(2), match1.group(4), match1.group(5))
        elif match2:
            title = 'S{:0>2}E{:0>2} - Episode {}'.format(match2.group(2), match2.group(4), match2.group(4))
        elif match3:
            title = 'S{:0>2}E{:0>2} - {}'.format(match3.group(1), match3.group(2), title)

        filename = "{} - {}".format(series, title)

    # strip invalid filename characters < > : " / \ | ? *
    filename = re.sub('[\<\>\:\"\/\\\|\?\*]', '-', filename)
    return filename

def add_episode(parent, ep_info):
    video_key = ep_info["episodeHouseNumber"]
    series_title = ep_info["seriesTitle"]
    title = ep_info.get("title", series_title)

    IviewEpisodeNode(title, series_title, parent, video_key)

class IviewEpisodeNode(Node):
    def __init__(self, title, series, parent, video_key):
        Node.__init__(self, title, parent)
        self.video_key = video_key
        self.series = series
        self.title = title
        self.can_download = True

    def find_hls_url(self, playlist):
        for video in playlist:
            if video["type"] == "program":
                for quality in ["hls-plus", "hls-high"]:
                    if quality in video:
                        return video[quality].replace("http:", "https:")
        raise Exception("Missing program stream for " + self.video_key)

    def find_captions_url(self, playlist):
        for video in playlist:
            if video["type"] == "program":
                for src in ["captions"]:
                    if src in video:
                        return video[src]["src-vtt"]
        raise Exception("Missing captions stream for " + self.video_key)

    def get_auth_details(self):
        with requests_cache.disabled():
            auth_doc = grab_xml(AUTH_URL)
        NS = {
            "auth": "http://www.abc.net.au/iView/Services/iViewHandshaker",
        }
        token = auth_doc.xpath("//auth:tokenhd/text()", namespaces=NS)[0]
        token_url = auth_doc.xpath("//auth:server/text()", namespaces=NS)[0]
        token_hostname = urllib.parse.urlparse(token_url).netloc
        return token, token_hostname

    def get_info(self):
        if self.info is None:
            self.info = grab_json(API_URL + "/programs/" + self.video_key)
            self.info['source_url_grabber'] = API_URL + "/programs/" + self.video_key
        return self.info

    def add_auth_token_to_url(self, video_url, token, token_hostname):
        parsed_url = urllib.parse.urlparse(video_url)
        hacked_url = parsed_url._replace(netloc=token_hostname, query="hdnea=" + token)
        video_url = urllib.parse.urlunparse(hacked_url)
        return video_url

    def download(self):
        #Store root directory
        root_dir = os.getcwd()
        series_dir = root_dir + "/" + self.series
        
        #Check for and create(if necessary) the series directory
        if not os.path.exists(series_dir):
            os.makedirs(series_dir)
        os.chdir(series_dir)
        
        info = self.get_info()

        if "playlist" in info:
            #get the playlist data
            playlists = info["playlist"]
        
            video_url = self.find_hls_url(playlists)
        else:
            os.chdir(root_dir)
            return False

        base_filename = descriptive_filename(self.series, self.title, video_url)
        
        #Store json result
        if not os.path.isfile(base_filename + ".json"):
            with open(base_filename + ".json", "w") as f:
                json.dump(info,f)
        
        #collect captions if they exists
        if not os.path.isfile(base_filename + ".vtt"):
            if "captions" in info:
                if info["captions"]==True:
                    caption_url = self.find_captions_url(playlists)
                    download_http (base_filename + ".vtt", caption_url, False)

        # Download video
        if not os.path.isfile(base_filename + ".mp4"):
            token, token_hostname= self.get_auth_details()
            video_url = self.add_auth_token_to_url(video_url, token, token_hostname)
            result = download_hls(base_filename + ".ts", video_url)

        os.chdir(root_dir)
        
        #return result
        return False

class IviewIndexNode(Node):
    def __init__(self, title, parent, url):
        Node.__init__(self, title, parent)
        self.url = url
        self.unique_series = set()

    def fill_children(self):
        logging.info("Retrieving URL: %s", self.url)
        info = grab_json(self.url)
        for key in ["carousels", "collections", "index"]:
            for collection_list in info[key]:
                if isinstance(collection_list, dict):
                    for ep_info in collection_list.get("episodes", []):
                        self.add_series(ep_info)

    def add_series(self, ep_info):
        title = ep_info["seriesTitle"]
        if title in self.unique_series:
            return
        self.unique_series.add(title)
        url = API_URL + "/" + ep_info["href"]
        IviewSeriesNode(title, self, url)

class IviewSeriesNode(Node):
    def __init__(self, title, parent, url):
        Node.__init__(self, title, parent)
        self.url = url

    def get_info(self):
        if self.info is None:
            ep_info = grab_json(self.url)
            series_slug = ep_info["href"].split("/")[1]
            series_url = API_URL + "/series/" + series_slug + "/" + ep_info["seriesHouseNumber"]
            logging.info("Grabbing JSON: %s",series_url)
            self.info = grab_json(series_url)
        return self.info

    def fill_children(self):
        info = self.get_info()
        for ep_info in info.get("episodes", []):
            add_episode(self, ep_info)

class IviewFlatNode(Node):
    def __init__(self, title, parent, url):
        Node.__init__(self, title, parent)
        self.url = url

    def fill_children(self):
        info = grab_json(self.url)
        for ep_info in info:
            add_episode(self, ep_info)


class IviewRootNode(Node):
    def load_categories(self):
        by_category_node = Node("By Category", self)
        def category(name, slug):
            IviewIndexNode(name, by_category_node, API_URL + "/category/" + slug)

        #TODO: dynamically read this list from http://iview.abc.net.au/api/category/
        category("Arts & Culture", "arts")
        category("Comedy", "comedy")
        category("Documentary", "docs")
        category("Drama", "drama")
        category("Education", "education")
        category("Lifestyle", "lifestyle")
        category("News & Current Affairs", "news")
        category("Panel & Discussion", "panel")
        category("Regional Australia", "regional")
        category("Sport", "sport")

    def load_channels(self):
        by_channel_node = Node("By Channel", self)
        def channel(name, slug):
            IviewIndexNode(name, by_channel_node, API_URL + "/channel/" + slug)

        #TODO: dynamically read this list from http://iview.abc.net.au/api/category/
        channel("ABC1", "abc1")
        channel("ABC2", "abc2")
        channel("ABC3", "abc3")
        channel("ABC4Kids", "abc4kids")
        channel("News", "news")
        channel("ABC Arts", "abcarts")
        channel("iView Exclusives", "iview")

    def load_featured(self):
        IviewFlatNode("Featured", self, API_URL + "/featured")

    def fill_children(self):
        self.load_categories()
        self.load_channels()
        self.load_featured()


def fill_nodes(root_node):
    IviewRootNode("ABC iView", root_node)

